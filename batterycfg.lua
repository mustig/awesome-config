-- Battery widget

batterycfg = {}
batterycfg.batt = "BAT1"
batterycfg.widget = widget({ type = "textbox", name = "batterycfg.widget", align = "right" })

--batterycfg_t = awful.tooltip({ objects = { batterycfg.widget },})
--batterycfg_t:set_text("Volume")

-- command must start with a space!
batterycfg.batterycommand = function (command)
       local fd = io.open("/sys/class/power_supply/" .. batterycfg.batt .. "/" .. command, "r")
       local status = fd:read()
       fd:close()
       return status
end

batterycfg.update = function ()
       batterycfg.widget.text = "BAT " .. batterycfg.batterycommand("capacity") .. " "
end

batterycfg.start = function ()
  local fd = io.open("/sys/class/power_supply/" .. batterycfg.batt, "r")
  if fd == nil then
    return false
  else
    fd:close()
    batterycfg.update()
    awful.hooks.timer.register(1, function () batterycfg.update() end)
    batterycfg.widget:buttons(awful.util.table.join(
      awful.button({ }, 1, function () batterycfg.update() end)
    ))
  end
end

batterycfg.start()
