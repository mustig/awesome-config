tags = {}
screen_layouts = { 2, 4, 2 }
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag({ 1, 2, 3, 4, 5, 6, 7, 8, 9 }, s, layouts[screen_layouts[s]])
end
